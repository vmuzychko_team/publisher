package net.eda.rest;

import net.eda.models.Event;
import net.eda.models.Payload;
import net.eda.publish.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publish")
public class EventController
{
    @Autowired
    private Publisher publisher;

    @PostMapping
    public long publishEvent(@RequestBody Payload payload) {
        Event event = new Event(payload);
        publisher.publish(event);
        return event.getId();
    }
}
