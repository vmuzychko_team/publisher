package net.eda;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import net.eda.publish.Publisher;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class PublisherApplication
{
    @Value("${host.name:localhost}")
    public String hostName;
    @Value("${new.events.exchange:new.events}")
    private String newEventsExchange;

    @Bean
    Connection connection()
    {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(hostName);
        try
        {
            return connectionFactory.newConnection();
        }
        catch (IOException | TimeoutException e)
        {
            throw new BeanCreationException("Unable to establish connection.", e);
        }
    }

    @Bean
    Channel channel(Connection connection)
    {
        try
        {
            return connection.createChannel();
        }
        catch (IOException e)
        {
            throw new BeanCreationException("Unable to create a channel.", e);
        }
    }

    @Bean(destroyMethod = "preDestroy")
    Publisher publisher(Connection connection, Channel channel)
    {
        return new Publisher(connection, channel, newEventsExchange);
    }

    public static void main(String[] args)
    {
        SpringApplication.run(PublisherApplication.class, args);
    }
}
