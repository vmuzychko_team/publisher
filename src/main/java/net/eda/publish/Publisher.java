package net.eda.publish;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import net.eda.exception.EdaException;
import net.eda.models.Event;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Service
public class Publisher
{
    public static final String ALL_EVENTS = "";
    public static final String FANOUT = "fanout";
    private Channel channel;
    private Connection connection;
    private String newEventsExchange;

    public Publisher(Connection connection, Channel channel, String newEventsExchange)
    {
        try
        {
            channel.exchangeDeclare(newEventsExchange, FANOUT);
            this.channel = channel;
            this.connection = connection;
            this.newEventsExchange = newEventsExchange;
        }
        catch (IOException e)
        {
            throw new EdaException("Unable to instantiate Publisher", e);
        }
    }

    public void preDestroy()
    {
        try
        {
            channel.close();
            connection.close();
        }
        catch (IOException | TimeoutException e)
        {
            throw new EdaException("Unable to destroy Publisher", e);
        }
    }

    public void publish(Event event)
    {
        try
        {
            channel.basicPublish(newEventsExchange, ALL_EVENTS, null, event.toByteArray());
        }
        catch (Exception e)
        {
            throw new EdaException("Unable to publish event to exchange", e);
        }
    }
}
